package com.company;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class HorseIsNotReady extends Exception {

    public HorseIsNotReady(String message) {

        super(message);

        PrintWriter out = null;
        try {
            FileOutputStream myFile = new FileOutputStream("message.txt");
            out = new PrintWriter(myFile);
            out.print(message);
            out.close();
        } catch (IOException e) {
            System.out.println("Don't write date to File");
        } finally {
            out.close();
        }
    }
}
