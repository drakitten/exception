package com.company;

public class Main {

    public static void main(String[] args) {

        Horse horse = new Horse("Sugar");

        horse.putHorseShoesOn();
        horse.putReinsOn();

        try {
            horse.ride();
        } catch (HorseIsNotReady e) {
            e.printStackTrace();
        }
    }
}
