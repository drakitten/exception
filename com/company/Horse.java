package com.company;

public class Horse {

    private String name;
    private boolean isSaddleOn;
    private boolean areReinsOn;
    private boolean areHorseShoesOn;

    public Horse(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void putSaddleOn() {
        isSaddleOn = true;
        System.out.println("Saddle is on!");
    }

    public void putHorseShoesOn() {
        areHorseShoesOn = true;
        System.out.println("Horseshoes are on!");
    }

    public void putReinsOn() {
        areReinsOn = true;
        System.out.println("Reins are on!");
    }

    public void ride() throws HorseIsNotReady {
        if (isSaddleOn && areReinsOn && areHorseShoesOn) {
            System.out.println("You are riding a " + name + "!");
        } else {
            throw new HorseIsNotReady("You cannot ride a horse, because " + name + " is not ready");
        }
    }
}
